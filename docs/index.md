---
layout: default
title: Aounify
---

# Using Aounify

Head on over to the [chrome web store][web-store] to download and install Aounify into Chrome.

Once added to chrome, browse to **any** website, and then look for Joseph Aoun's face in the top right corner and give it a click.

[web-store]: https://chrome.google.com/webstore/detail/aounify/nlpngjpnabbacnkijamacdeeplodbfmd
